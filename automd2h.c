/**
 * @author yassine Hasnaoui (HASY04089702)
 * @author Philippe (LEBP11129502)
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
// sys calls
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/inotify.h>
#include <signal.h>

//directroy management
#include <libgen.h>

#define EVENT_SIZE (sizeof(struct inotify_event))
#define EVENT_BUF_LEN (1024 * (EVENT_SIZE + 16))
#define BUF_LEN (1024 * (EVENT_SIZE + 16))

#define USAGE "\n\
Usage: [-h|--help] [-t| ] [-n| ] [-w| ] [-r| ] [-f| ]\n\
\n\
automd2h convertit les fichiers au format Markdown en fichiers au format HTML.\n\
\n\
 Arguments optionnel:\n\
  -h,                         Shows this help message and exit\n\
                              \n\
  -t,                         Avec l'option -t. La date de dernière modification des fichiers\n\
                              est utilisée pour savoir s'il faut reconvertir. Si le fichier source \n\
                              est plus récent que le ficher .html cible associé, ou si le fichier. \n\
                              html cible n'existe pas, alors il y a conversion. Si la date est identique \n\
                              ou si le fichier .html cible est plus récent, alors il n'y a pas de conversion.\n\
                              \n\
  -n,                         L'option -n désactive l'utilisation de pandoc, à la place, la liste des chemins\n\
                              des fichiers sources à convertir sera affichée (un par ligne).\n\
                              Combiné avec -n, l'option -t n'affiche que les fichiers \n\
                              sources effectivement à convertir.\n\
                              \n\
  -w,                         Avec l'option -w, automd2h bloque et surveille les modifications des fichiers \n\
                              et des répertoires passés en argument. Lors de la modification d'un fichier source,\n\
                              celui-ci est automatiquement reconverti. Si dans un répertoire surveillé un fichier .md \n\
                              apparait, est modifié, est déplacé ou est renommé, celui-ci aussi est automatiquement converti.\n\
                              \n\
  -r,                         L'option -r visite les répertoires récursivement et cherche les fichiers dont l'extension est .md pour les convertir.\n\
                              \n\
  -f,                         Par défaut, avec -w, les fichiers ne sont convertis que si une modification future est détectée.\n\
                              Combiné avec -w, l'option -f force la conversion immédiate des fichiers trouvés puis surveille les modifications futures.\n\
"
/**
 * The status of the program.
 */
enum Status
{
    OK,         /**< Everything is alright */
    WRONG_VALUE /**< Wrong value */
};

enum Format
{
    txt,
    markdown,
    html,
    another,
    Directory
};

enum Options
{
    t,
    n,
    r,
    w,
    f,
    no_option,
    Optionerror
};

struct File
{
    /* data */
    enum Format format;
    time_t time;        //the last modification time
    char filename[300]; //The name of the file
};

/**
 * The parsed arguments.
 */
struct Arguments
{
    enum Status status; /**< The status of the parsing */
    enum Options option1, option2, option3, option4;
    bool Default; /**< Default Args  */
    int argv_index;
    int num_files;
    int num_options;
    struct File files[200]; // Array of Files to convert. It can be unlimited
};

// Check if file exists in Current repository
bool file_exist(char *filePath)
{
    return access(filePath, F_OK) != -1;
};

// Function to replace a string with another
char *replaceWord(const char *s, const char *oldW, const char *newW)
{
    char *result;
    int i, cnt = 0;
    int newWlen = strlen(newW);
    int oldWlen = strlen(oldW);

    // Counting the number of times old word
    // occur in the string
    for (i = 0; s[i] != '\0'; i++)
    {
        if (strstr(&s[i], oldW) == &s[i])
        {
            cnt++;

            // Jumping to index after the old word.
            i += oldWlen - 1;
        }
    }

    // Making new string of enough length
    result = (char *)malloc(i + cnt * (newWlen - oldWlen) + 1);

    i = 0;
    while (*s)
    {
        // compare the substring with the result
        if (strstr(s, oldW) == s)
        {
            strcpy(&result[i], newW);
            i += newWlen;
            s += oldWlen;
        }
        else
            result[i++] = *s++;
    }

    result[i] = '\0';
    return result;
}
/**
 * File Validation functions.
 */

char *get_filename_ext(char *filename)
{
    char *dot = strrchr(filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot;
}

bool is_Markdown(char *filename)
{
    return strcmp(get_filename_ext(filename), ".md") == 0;
}

bool is_HTML(char *filename)
{
    return strcmp(get_filename_ext(filename), ".html") == 0;
}

bool is_directory(char *Dir_name)
{
    bool is_directory = false;
    DIR *dir;

    dir = opendir(Dir_name);
    if (dir == NULL)
    {
        // printf("Couldn't open dir\n");
    }
    else
    {
        //printf("Opened dir\n");
        is_directory = true;
    }

    if (dir != NULL)
        closedir(dir);

    return is_directory;
}
/**
 * Option Validation functions.
 */
bool is_Option(char *option)
{
    return strstr(option, "-") != NULL && strlen(option) == 2;
}
bool is_Option_t(char *option)
{
    return strstr(option, "-t") != NULL;
}
bool is_Option_n(char *option)
{
    return strstr(option, "-n") != NULL;
}
bool is_Option_r(char *option)
{
    return strstr(option, "-r") != NULL;
}
bool is_Option_w(char *option)
{
    return strstr(option, "-w") != NULL;
}
bool is_Option_f(char *option)
{
    return strstr(option, "-f") != NULL;
}
enum Options option_detection(char *option)
{
    enum Options Detected_option = no_option;
    if (is_Option_t(option))
    {

        Detected_option = t;
    }
    else if (is_Option_n(option))
    {

        Detected_option = n;
    }
    else if (is_Option_r(option))
    {

        Detected_option = r;
    }
    else if (is_Option_w(option))
    {

        Detected_option = w;
    }
    else if (is_Option_f(option))
    {

        Detected_option = f;
    }
    else
    {
        Detected_option = Optionerror;
    }

    return Detected_option;
}

void print_args(struct Arguments *arguments)
{
    printf("Arguments  \n");
    switch (arguments->option1)
    {
    case n:
        printf("Arg1 : n \n");
        break;
    case t:
        printf("Arg1 : t \n");
        break;
    case w:
        printf("Arg1 : w \n");
        break;
    case f:
        printf("Arg1 : f \n");
        break;
    case no_option:
        printf("Arg1 : no_option \n");
        break;

    default:
        break;
    }

    switch (arguments->option2)
    {
    case n:
        printf("Arg2 : n \n");
        break;
    case t:
        printf("Arg2 : t \n");
        break;
    case w:
        printf("Arg2 : w \n");
        break;
    case f:
        printf("Arg2 : f \n");
        break;
    case no_option:
        printf("Arg2 : no_option \n");
        break;

    default:
        break;
    }

    switch (arguments->option3)
    {
    case n:
        printf("Arg3 : n \n");
        break;
    case t:
        printf("Arg3 : t \n");
        break;
    case w:
        printf("Arg3 : w \n");
        break;
    case f:
        printf("Arg3 : f \n");
        break;
    case no_option:
        printf("Arg3 : no_option \n");
        break;

    default:
        break;
    }

    switch (arguments->option4)
    {
    case n:
        printf("Arg4 : n \n");
        break;
    case t:
        printf("Arg4 : t \n");
        break;
    case w:
        printf("Arg4 : t \n");
        break;
    case f:
        printf("Arg4 : f \n");
        break;
    case no_option:
        printf("Arg4 : no_option \n");
        break;

    default:
        break;
    }
}

char *concatenate_file_extension(char *filePath)
{
    char *newFileName = (char *)malloc(255);
    if (file_exist(filePath))
    {
        if (is_Markdown(filePath))
        {
            //copy all except .md
            newFileName = replaceWord(filePath, ".md", ".html");
        }
        else if (is_directory(filePath) == false)
        {
            strcpy(newFileName, filePath);
            strcat(newFileName, ".html");
        }
    }
    return newFileName;
}

//Check if converted document has a newer version (option t)
bool has_new_doc_version(time_t sourceFile, time_t destFile)
{
    return sourceFile > destFile;
}

//Check if documents has a new version to convert
bool file_needs_conversion(char *filename)
{
    bool convert = false;
    struct stat attrib;
    struct stat newAttrib;

    char *newFileName = concatenate_file_extension(filename);

    //Checking if the given file exists and if its a .md
    if (file_exist(filename))
    {
        //getting the file stats
        if (stat(filename, &attrib) == 0)
        {
            //if there is no errors, check if his htmk version exists
            if (file_exist(newFileName))
            {
                //if the html file alredy exists, we proceed
                if (stat(newFileName, &newAttrib) == 0)
                {
                    // Verify if there is a newer version of the source file
                    if (has_new_doc_version(attrib.st_mtime, newAttrib.st_mtime))
                    {
                        //If there is, a convertion is needed
                        convert = true;
                    }
                }
            }
            else
            {
                //destFile does not exists, need to convert the source file
                convert = true;
            }
        }
    }
    else
    {
        convert = false;
        //printf("The file %s does not exists or is not .md\n\n", filename);
    }

    return convert;
};

//pritn all txt and md file in a directory (option n)
void print_current_directory(char *currentDir, bool checkTime)
{
    struct dirent *d;

    //printf("printing current directory...\n");
    DIR *dir = opendir(currentDir);
    if (dir == NULL)
    {
        fprintf(stderr, "Can't access directory\n");
    }
    while ((d = readdir(dir)) != NULL)
    {
        if (is_Markdown(d->d_name))
        {
            if (!checkTime || file_needs_conversion(d->d_name))
            {
                printf("%s\n", d->d_name);
				fflush(stdout);
            }
        }
    }
    closedir(dir);
}

void print_arguments_files(struct Arguments *arguments, bool checkTime)
{
    for (int i = 0; i < arguments->num_files; i++)
    {
        if (is_directory(arguments->files[i].filename))
        {
            print_current_directory(arguments->files[i].filename, checkTime);
        }
        else
        {
			if (file_needs_conversion(arguments->files[i].filename) || checkTime == false){
				printf("%s\n", arguments->files[i].filename);
				fflush(stdout);

			}
        }
    }
}

int Pandoc(char *file)
{
    //printf("Pandoc is trying to convert the file...\n");
    // Forking
    pid_t c_pid;
    c_pid = fork();

    if (c_pid == -1)
    {
        perror("Fork Error");
    }
    // child process because return value zero
    else if (c_pid == 0)
    {
        //Pandoc will run here.
        char *output = concatenate_file_extension(file);
        char *ls_args[] = {"pandoc", file, "-o", output, NULL};

        //calling pandoc
        if (file_exist(file))
        {
            execvp(ls_args[0], ls_args);
        }
        else
        {
            //Error Handeler
            perror("ENOENT");
            exit(EXIT_FAILURE);
        }
        return 0;
        //exit(EXIT_SUCCESS);
    }
    else
    {
        //parent
        int status;

        //Wait for the child whose process ID is equal to the value of pid.
        waitpid(c_pid, &status, 0);
        if (WIFEXITED(status))
        {
            int exit_status = WEXITSTATUS(status);
            if (exit_status != EXIT_SUCCESS)
            {
                exit(EXIT_FAILURE);
            }
        }
    }
    return 0;
}

// Check if the user entered the same option twice.
bool Check_Duplicates(enum Options OptionArray[])
{

    bool DuplicateOption = false;

    for (int i = 0; i < 4 - 1; i++)
    {
        for (int j = i + 1; j < 4; j++)
        {
            if (OptionArray[i] != no_option && OptionArray[i] == OptionArray[j])
            {
                DuplicateOption = true;
            }
        }
    }
    return DuplicateOption;
}

//Convert all md (ASKED SPECIFICALLY) files inside Directory if there is no html version of them
int Convert_Directory(char *Dir, bool checktime)
{
    DIR *Directory;
    struct dirent *entry;
    struct stat filestat;

    Directory = opendir(Dir);
    if (Directory == NULL)
    {
        return (0); // leave
    }
    else
    {
        /* Read directory entries */
        while ((entry = readdir(Directory)))
        {
            char fullname[257];
            sprintf(fullname, "%s/%s", Dir, entry->d_name);
            stat(fullname, &filestat);

            if (S_ISDIR(filestat.st_mode))
            {
                //printf("%4s: %s\n", "Dir", fullname);
            }
            else
            {
                //its a file
                if (is_Markdown(fullname) && (file_needs_conversion(fullname) || checktime == false))
                {
                    if (Pandoc(fullname) == 1)
                    {
                        return 1;
                    }
                }
            }
        }
        closedir(Directory);
    }
    return (0);
}

bool no_options_entered(enum Options OptionArray[])
{
    return OptionArray[0] == no_option && OptionArray[1] == no_option && OptionArray[2] == no_option && OptionArray[3] == no_option;
}

// The following functions checks if a specific option is in the arguments structure

bool Option_n(struct Arguments *arguments)
{
    return arguments->option1 == n || arguments->option2 == n || arguments->option3 == n || arguments->option4 == n;
}

bool Option_t(struct Arguments *arguments)
{
    return arguments->option1 == t || arguments->option2 == t || arguments->option3 == t || arguments->option4 == t;
}

bool Option_w(struct Arguments *arguments)
{
    return arguments->option1 == w || arguments->option2 == w || arguments->option3 == w || arguments->option4 == w;
}

bool Option_r(struct Arguments *arguments)
{
    return arguments->option1 == r || arguments->option2 == r || arguments->option3 == r || arguments->option4 == r;
}

bool Option_f(struct Arguments *arguments)
{
    return arguments->option1 == f || arguments->option2 == f || arguments->option3 == f || arguments->option4 == f;
}
// To review
void free_arguments(struct Arguments *arguments)
{
    free(arguments);
}

void Print_num_Options(struct Arguments *arguments)
{
    printf("Number of options entered :%d \n", arguments->num_options);
}

int Watch(struct Arguments *arguments, bool usePandoc){
    //  keep watching EVERY file or directories given as arguments
    while (true)
    {
        int length, i = 0;
        int fd;
        int wd;
        char buffer[EVENT_BUF_LEN];
		char target[310];

        /*creating the INOTIFY instance*/
        fd = inotify_init();

        /*checking for error*/
        if (fd < 0)
        {
            perror("inotify_init");
            exit(EXIT_FAILURE);
        }

		for (int file = 0; file < arguments->num_files; file++){
            //check if the argument is a directory or not
			if(is_directory(arguments->files[file].filename) == false){
                //copy of the file path
				char *dup = strdup(arguments->files[file].filename);
                //extract the name of the directory
				char *dir = dirname(dup);
                //add the file to watch in the watch list
	    		wd = inotify_add_watch(fd, dir, IN_MODIFY);
			}
			else {
                //its a directory, add it to the watch list
				wd = inotify_add_watch(fd, arguments->files[file].filename, IN_CREATE | IN_MODIFY | IN_MOVED_TO);	
			}
		}

        length = read(fd, buffer, EVENT_BUF_LEN);

        /*checking for error*/
        if (length < 0)
        {
            perror("read");
        }
        /*actually read return the list of change events happens. Here, read the change event one by one and process it accordingly.*/
        while (i < length)
        {
            struct inotify_event *event = (struct inotify_event *)&buffer[i];
            if (event->len)
            {
                //If something was detected run this code
                if (event->mask & (IN_CREATE | IN_MODIFY | IN_MOVED_TO | IN_MOVED_FROM))
                {
                    //Directory was created,modified or moved IN the given Directory
                    if (event->mask & IN_ISDIR)
                    {

                    }
                    //file was created,modified or moved IN the given Directory
                    else
                    {
                        //Checks if the file detected was in our arguments target
						for (int file = 0; file < arguments->num_files; file++){
                            // if the given argument is a directory, create the Target Path of the modified file so
                            // pandoc knows where and what to convert
							if(is_directory(arguments->files[file].filename)){
                                //creating the path of the file event
								sprintf(target, "%s/%s", arguments->files[file].filename, event->name);	
							}
							else{
                                //if the given argument is not a directory it means that the file is in the root dir
                                // then we only need the name of the event file to call pandoc.
								strcpy(target, arguments->files[file].filename);
							}
                            //printf("%s\n",event->name);

                            //now we check if the target matches the event, if the modified file is the target,
                            //then we have to convert that file using pandoc. The target file have to exists.
							if(strstr(target, event->name) != NULL && file_exist(target)){
                                //printf("%s\n", target);								
                                if(usePandoc){
									Pandoc(target);
								}
                                //option -n, desactivate the use of pandoc and prints the modifications
								else{
									printf("%s\n", target);
									fflush(stdout);

								}
							}
						}
                    }                
                }
            }
            i += EVENT_SIZE + event->len;
        }
        inotify_rm_watch(fd, wd);
        /*closing the INOTIFY instance*/
        close(fd);    
    }

    return 0;
}

int RecursiveConversion(char *dir, bool checkTime){
	DIR *Directory;
	struct dirent *entry;
    struct stat filestat;
	Directory = opendir(dir);

    if (Directory == NULL){
		return (1); // leave
    }

	/* Read directory entries */
    while ((entry = readdir(Directory)))
    {
        char fullname[257];
        sprintf(fullname, "%s/%s", dir, entry->d_name);
        stat(fullname, &filestat);

        //Checking if we are dealing with a file or a directory
        if (S_ISDIR(filestat.st_mode))
        {
            //its a dir
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) // to not infinite loop
            {
                // Recursion happens here
                RecursiveConversion(fullname, checkTime);
            }
        }
        else
        {
            //its a file
			if(is_Markdown(fullname) && (checkTime == false || file_needs_conversion(fullname))){

				if (Pandoc(fullname) == 1)
	            {
	                return 1;
	            }
			}
        }
    }
	return 0;
}

int addRecursiveWatcher(struct Arguments *arguments){
	DIR *Directory;
	struct dirent *entry;
    struct stat filestat;
	Directory = opendir(arguments->files[arguments->num_files-1].filename);

    if (Directory == NULL){
		return (1); // leave
    }

	/* Read directory entries */
    while ((entry = readdir(Directory)))
    {
        char fullname[600];
        sprintf(fullname, "%s/%s", arguments->files[arguments->num_files-1].filename, entry->d_name);
        stat(fullname, &filestat);

        //Checking if we are dealing with a file or a directory
        if (S_ISDIR(filestat.st_mode))
        {
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) // to not infinite loop
        	{
				//printf("%s\n", fullname);
				strcpy(arguments->files[arguments->num_files].filename, fullname);
				arguments->num_files++;
				addRecursiveWatcher(arguments);
			}
		}
	}
	return 0;
}

void No_arg_Failure(int argc)
{
    if (argc == 1)
    {
        exit(EXIT_FAILURE);
    }
}
// launching the program with no option entered.
int launch_with_no_options(struct Arguments *arguments)
{

    for (int i = 0; i < arguments->num_files; i++)
    {
        //   if the current argument is a file
        if (is_directory(arguments->files[i].filename) == false)
        {
            if (file_exist(arguments->files[i].filename))
            {
                if (Pandoc(arguments->files[i].filename) == 1)
                {
                    return 1;
                }
            }
            else
            {
                perror("ENOENT");
                exit(EXIT_FAILURE);
            }
        }
        //   if the current argument is a Directory
        else if (is_directory(arguments->files[i].filename) == true)
        {
            if (Convert_Directory(arguments->files[i].filename, false) == 1)
            {
                return 1;
            }
        }
    }
    return 0;
}
// launching the program with options
int launch_with_options(struct Arguments *arguments)
{
	bool usePandoc = true;
	bool checkTime = false;
	bool forceConversion = false;
	bool watch = false;
	bool recursive = false;

    if(Option_n(arguments)){
		usePandoc = false;
	}
	if(Option_t(arguments)){
		checkTime = true;
	}
	if(Option_f(arguments)){
		forceConversion = true;
	}
	if(Option_w(arguments)){
		watch = true;
	}
	if(Option_r(arguments)){
		recursive = true;
	}
	
	if(recursive == true){
		if(watch == false || forceConversion == true){		
			for (int file = 0; file < arguments->num_files; file++){
				if (is_directory(arguments->files[file].filename) == true)
				{
					RecursiveConversion(arguments->files[file].filename, checkTime);	
				}
			}	
		}
		if(watch == true){
			//add all subDirectories in arg struct
			addRecursiveWatcher(arguments);
			Watch(arguments, usePandoc);
		}
	}
	else if(usePandoc == false && watch == false){
		print_arguments_files(arguments, checkTime);
	}
	else{
		if(watch == false || forceConversion == true){
			for (int file = 0; file < arguments->num_files; file++){
				if (is_directory(arguments->files[file].filename) == true)
				{
					if (Convert_Directory(arguments->files[file].filename, checkTime) == 1)
					{
						return 1;
					}
				}
				else if (is_directory(arguments->files[file].filename) == false){
					if (file_exist(arguments->files[file].filename)){
						if (file_needs_conversion(arguments->files[file].filename) || !checkTime){
						    if (Pandoc(arguments->files[file].filename) == 1){
						        return 1;
						    }
						}
					}
					else{
						perror("ENOENT");
						exit(EXIT_FAILURE);
					}
				}
			}
		}
		if(watch == true){
        	Watch(arguments, usePandoc);       
		}
	}
    return 0;
}

int lauchProgram(struct Arguments *arguments)
{
    //Array of options
    enum Options OptionArray[5];
    OptionArray[0] = arguments->option1;
    OptionArray[1] = arguments->option2;
    OptionArray[2] = arguments->option3;
    OptionArray[3] = arguments->option4;
    OptionArray[4] = no_option;

    if (Check_Duplicates(OptionArray))
    {
        //fprintf(stderr, "Error duplicates options.\n");
        return 1;
    }

    //if no option is entered, we convert files entered if there is no html version of them
    if (no_options_entered(OptionArray))
    {
        if (launch_with_no_options(arguments) == 1)
        {
            return 1;
        }
        return 0;
    }
	else if (launch_with_options(arguments) == 1){
		return 1;
	}
    return 0;
}
void initialise_Arguments(struct Arguments *arguments)
{
    arguments->option1 = no_option;
    arguments->option2 = no_option;
    arguments->option3 = no_option;
    arguments->option4 = no_option;
    arguments->argv_index = 1;
    arguments->num_files = 0;
    arguments->num_options = 0;
}
struct Arguments *parse_arguments(int argc, char *argv[])
{
    //fails if no arguments
    No_arg_Failure(argc);

    //printf("Parsing Arguments..\n");
    struct Arguments *arguments = malloc(sizeof(struct Arguments));
    initialise_Arguments(arguments);

    // ---Option detection Part ---
    if (is_Option(argv[1]))
    {
        for (int i = 1; i < argc; i++)
        {
            //counting number of options and incrementing the arg index
            if (is_Option(argv[i]))
            {
                arguments->num_options++;
                arguments->argv_index++;
            }
        }
        //if we have one option as argument
        if (arguments->num_options == 1)
        {
            arguments->option1 = option_detection(argv[1]);
            arguments->status = OK;
        }
        //if we have two option as argument
        else if (arguments->num_options == 2)
        {
            arguments->option1 = option_detection(argv[1]);
            arguments->option2 = option_detection(argv[2]);
            arguments->status = OK;
        }
        else if (arguments->num_options == 3)
        {
            arguments->option1 = option_detection(argv[1]);
            arguments->option2 = option_detection(argv[2]);
            arguments->option3 = option_detection(argv[3]);
            arguments->status = OK;
        }
        else if (arguments->num_options == 4)
        {
            arguments->option1 = option_detection(argv[1]);
            arguments->option2 = option_detection(argv[2]);
            arguments->option3 = option_detection(argv[3]);
            arguments->option4 = option_detection(argv[4]);
            arguments->status = OK;
        }

        // ---File detection Part ---
    }
    //Looping through the rest of arguments (files)
    for (int i = arguments->argv_index; i < argc; i++)
    {
        if (is_Markdown(argv[arguments->argv_index]))
        {

            strncpy(arguments->files[arguments->num_files].filename, argv[arguments->argv_index], sizeof(arguments->files[arguments->num_files].filename));
            arguments->files[arguments->num_files].filename[sizeof(arguments->files[arguments->num_files].filename) - 1] = '\0';
            //printf("You want me to convert a md file (%s) \n",argv[arguments->argv_index]);
            arguments->files[arguments->num_files].format = markdown;
            arguments->status = OK;
            arguments->num_files++;
        }
        else if (is_HTML(argv[arguments->argv_index]))
        {
            //printf("You want me to convert a html file \n");
            strncpy(arguments->files[arguments->num_files].filename, argv[arguments->argv_index], sizeof(arguments->files[arguments->num_files].filename));
            arguments->files[arguments->num_files].filename[sizeof(arguments->files[arguments->num_files].filename) - 1] = '\0';
            arguments->files[arguments->num_files].format = html;
            arguments->status = OK;
            arguments->num_files++;
        }
        else if (is_directory(argv[arguments->argv_index]))
        {
            //printf("You want me to convert files inside (%s) directory \n", argv[arguments->argv_index]);
            strncpy(arguments->files[arguments->num_files].filename, argv[arguments->argv_index], sizeof(arguments->files[arguments->num_files].filename));
            arguments->files[arguments->num_files].filename[sizeof(arguments->files[arguments->num_files].filename) - 1] = '\0';
            arguments->files[arguments->num_files].format = Directory;
            arguments->status = OK;
            arguments->num_files++;
        }
        else
        {
            //printf("You want me to convert a another file \n");
            strncpy(arguments->files[arguments->num_files].filename, argv[arguments->argv_index], sizeof(arguments->files[arguments->num_files].filename));
            arguments->files[arguments->num_files].filename[sizeof(arguments->files[arguments->num_files].filename) - 1] = '\0';
            arguments->files[arguments->num_files].format = another;
            arguments->status = OK;
            arguments->num_files++;
        }
        //next argument
        arguments->argv_index++;
    }

    arguments->status = OK;
    return arguments;
}

int main(int argc, char *argv[])
{
    //printf(USAGE);
    struct Arguments *arguments = parse_arguments(argc, argv); //takes the arguments in the structure
    if (arguments->status != OK)
    {
        //fprintf(stderr, "failed to read arguments\n");
        return 1;
    }
    else
    {
        // All good
        if (lauchProgram(arguments) == 1)
        {
            return 1;
        }
    }
    free_arguments(arguments);
    return 0;
}
